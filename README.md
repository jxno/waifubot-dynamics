# WaifuDyn

Thankfully the script is quite simple.

All you need to do is copy the example `.env.example` file to
`.env` & insert your API token from Facebook where it says "Whatever"

If you read this before I make the change, currently it selects images by
webp on line 55, just change this to .jpg when you need.

Make a GitLab account, clone this repo, remove the `.git` folder
& push this to your own repo instead. Make sure your repo is __private__
so that your API key isn't exposed (can't stress this importance enough)

The pipeline will run automatically once you make the push,
& your bot will (hopefully) run.

Much love. 🕸️

